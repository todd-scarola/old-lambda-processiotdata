const { Client } = require('pg');

exports.executeQuery = async(sql, params) => {
    //console.log('sql: %s params: %s', sql, params);

    //db Client
    var client = new Client();

    try {
        await client.connect();
        var result = await client.query(sql, params);
        //console.log('sql result: %s', result);
    }
    catch (error) {
        if (error.code === 23505) {
            console.log('Duplicate Key');
        }
        else {
            console.error(error);
        }
    }

    //cleanup connection
    await client.end();

    return result;
};


exports.addContact = async(contact) => {

    var sql = 'INSERT INTO contacts ( \
    tag_a, tag_b, \
    event_start, event_end, \
    range, duration, \
    status, updated_at) \
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8) \
    RETURNING id';

    var params = [
        contact.tag_a,
        contact.tag_b,
        new Date(contact.event_start * 1000), //in seconds
        new Date(contact.event_end * 1000), //in seconds
        contact.range,
        contact.duration,
        contact.status,
        new Date(contact.updated_at)
    ];

    return await this.executeQuery(sql, params);
};


exports.updateContact = async(contact) => {
    var sql = 'UPDATE contacts SET event_end=$1, range=$2, duration=$3, status=$4, updated_at=$5 WHERE id=$6';

    var params = [
        new Date(contact.event_end * 1000),
        contact.range,
        contact.duration,
        contact.status,
        new Date(contact.updated_at),
        contact.id
    ];

    var result = await this.executeQuery(sql, params);

    if (result.rowCount > 0) { return true }

    return false;
};


//get the currently open contact between 2 tags
exports.getContact = async(tagA, tagB) => {
    var sql = "SELECT * FROM contacts WHERE tag_a=$1 AND tag_b=$2 AND status=$3 ORDER BY event_start DESC LIMIT 1";

    var params = [
        tagA,
        tagB,
        "open"
    ];

    var result = await this.executeQuery(sql, params);

    if (result.rowCount > 1) {
        console.error("MULTIPLE OPENS: %s %s", tagA, tagB);
    }


    if (result.rowCount == 1) {
        
        //convert dates back to epoch
        var contact = result.rows[0];
        contact.event_start = new Date(contact.event_start).getTime() / 1000;
        contact.event_end = new Date(contact.event_end).getTime() / 1000;
        return contact;
    }

    return false;
};


exports.saveContact = async(contact) => {

    contact.updated_at = Date.now();

    //determine if this is an update or an insert
    if (contact.hasOwnProperty('id')) {
        await this.updateContact(contact);
    }
    else {
        await this.addContact(contact);
    }
};
