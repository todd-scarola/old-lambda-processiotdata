'use strict';
const CryptoJS = require("crypto-js");

exports.decrypt = async(key, data) => {

    var decrypted = CryptoJS.AES.decrypt(
        CryptoJS.lib.CipherParams.create({
            ciphertext: CryptoJS.enc.Hex.parse(data)
        }),
        CryptoJS.enc.Hex.parse(key), {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.NoPadding
        });

    return decrypted.toString();
};


exports.generateKey = async(mac) => {
    // Key is the mac address of device e.g. AABBCCDDEEFF and this function will generate Decryption key from Mac key.
    var firstOneChar = mac.charAt(0);
    var firstTwoChar = mac.charAt(2);
    var keyOne = firstOneChar.concat(firstTwoChar, firstOneChar, "1", firstTwoChar, "2");
    var secondOneChar = mac.charAt(1);
    var secondTwoChar = mac.charAt(3);
    var keyTwo = secondOneChar.concat(secondTwoChar, secondOneChar, "3", secondTwoChar, "4");
    var thirdOneChar = mac.charAt(4);
    var thirdTwoChar = mac.charAt(6);
    var keyThree = thirdOneChar.concat(thirdTwoChar, thirdOneChar, "5", thirdTwoChar, "6");
    var fourthOneChar = mac.charAt(5);
    var fourthTwoChar = mac.charAt(7);
    var keyFourth = fourthOneChar.concat(fourthTwoChar, fourthOneChar, "7", fourthTwoChar, "8");
    var fifthOneChar = mac.charAt(8);
    var fifthTwoChar = mac.charAt(10);
    var keyFifth = fifthOneChar.concat(fifthTwoChar, fifthOneChar, "9");
    var sixthOneChar = mac.charAt(9);
    var sixthTwoChar = mac.charAt(11);
    var keySixth = sixthOneChar.concat(sixthTwoChar, sixthOneChar, "A");

    return keyOne.concat(keyTwo, keyThree, keyFourth, keyFifth, keySixth);
};