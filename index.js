//inboud message is an SQS trigger
exports.handler = async(event) => {

   var responses = [];

   //need to figure out which property to ID SQS messages
   if (event.hasOwnProperty('Records'))
   //SQS event
   {
      for (const record of event.Records) {
         var body = record.body;
         try {
            body = JSON.parse(body);
         }
         catch (e) { console.error(e); }

         responses.push(await this.processMessage(body));
      }
      return responses;
   }
   else
   //normal JSON request
   {
      return await this.processMessage(event);
   }
};

exports.processMessage = async(event) => {

   //load the file based on device type and message type
   var helper = null;


   //might want to scrub inbound json to all lower case, 
   //there are mixed examples provided 

   //Laird message
   if (event.hasOwnProperty('device_type') && event.hasOwnProperty('message_type')) {
      helper = require('./' + event.device_type + '/' + event.message_type + '/main.js');
   }
   //EP-POM 100 message
   else if (event.hasOwnProperty('R_Record_Count')) {
      helper = require('./T30G/notifyData/main.js');
   }

   //if we mapped a helper, process the event
   if (helper) {
      return await helper.processData(event);

      //write result to db layer

      //send SNS notice
   }

};
