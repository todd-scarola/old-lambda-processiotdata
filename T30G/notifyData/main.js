'use strict';
const sf = require('../../sharedFunctions.js');
//const dynamo = require('../../sharedDynamo.js');

exports.processData = async(event) => {
   console.log('POM Notify Data: %j', event);

   //strip trailing ';'
   event.Records = event.Records.slice(0, -1);

   //get encrypted records and remove
   const records = event.Records.split(';');
   delete event.Records;

   //extract record count and strip
   event.Data_Count = parseInt(records[0].substring(0, 2), 16);
   records[0] = records[0].substring(2);

   event.Records = [];
   for (let index = 0; index < records.length; index++) {
      var record = await this.parseRecord(event, records[index]);
      if (record) {
         event.Records.push(record);
      }
   }
   
   //write to Dynamo
   //await dynamo.updateExposure(event);

   return event;

};

exports.parseRecord = async(event, record) => {


   //strip 1st 32 characters
   record = record.substring(0, 32);
   
   //calculate decrypt key
   var key = await sf.generateKey(event.Device_MAC);

   //decrypt data
   var hex_data = await sf.decrypt(key, record);

   var data = {};
   data.raw_encrypted = record;
   data.raw_decrypted = hex_data;
   data.key = key;
   data.mac = hex_data.substring(0, 12);
   data.distance = parseInt(hex_data.substring(12, 14), 16);

   var day = parseInt(hex_data.substring(14, 16), 16);
   var month = parseInt(hex_data.substring(16, 18), 16);
   var year = 2000 + parseInt(hex_data.substring(18, 20), 16);
   var hour = parseInt(hex_data.substring(20, 22), 16);
   var minute = parseInt(hex_data.substring(22, 24), 16);
   var second = parseInt(hex_data.substring(24, 26), 16);

   data.event_time = new Date(Date.UTC(year, month, day, hour, minute, second, 0)).toISOString();

   data.battery = parseInt(hex_data.substring(26, 28), 16);
   data.firmware = parseInt(hex_data.substring(28, 30), 16);

   return data;

};
