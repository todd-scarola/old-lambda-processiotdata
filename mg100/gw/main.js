'use strict';
const DDB = require('../../sharedDynamo.js');
const PG = require('../../sharedPostgres.js');

const MFG = 'Laird';
const DDB_DEVICE_TABLE = process.env.DDB_DEVICE_TABLE;
const DDB_EXPOSURE_TABLE = process.env.DDB_EXPOSURE_TABLE;

exports.processData = async(event) => {

   //add timestamp
   event.process_time = Date.now();

   //parse Laird packet
   event.message_body = await this.parseMessage(event.message_body);
   //console.log('message body: %j', event.message_body);

   if (event.message_body) {
      //store data
      //await this.updateGateway(event);
      //await this.updateTagA(event);
      //await this.updateTagB(event);

      await this.updateContacts(event);

   }

   return event;
};

exports.updateGateway = async(event) => {
   var device = {
      device_id: event.device_id,
      device_type: 'gateway',
      manufacturer: MFG,
      model: 'MG100',
      last_event: event.message_body.lastUploadTime,
      last_update: Date.now()
   };

   await DDB.updateDevice(DDB_DEVICE_TABLE, device);

};

exports.updateTagA = async(event) => {
   var device = {
      device_id: event.message_body.deviceId,
      device_type: 'tag',
      manufacturer: MFG,
      model: 'BT510',
      last_event: event.message_body.lastUploadTime,
      last_update: Date.now(),
      firmware: event.message_body.fwVersion,
      battery: await this.parseBattery(Number(event.message_body.batteryLevel))
   };

   await DDB.updateDevice(DDB_DEVICE_TABLE, device);

};

exports.updateTagB = async(event) => {
   for (let index = 0; index < event.message_body.entries.length; index++) {
      var entry = event.message_body.entries[index];

      //sometimes the timestamp is 0 of the tag has an issue
      if (entry.timestamp == 0) { entry.timestamp = event.message_body.lastUploadTime; }

      var device = {
         device_id: entry.serial,
         device_type: 'tag',
         manufacturer: MFG,
         model: 'BT510',
         last_event: entry.timestamp,
         last_update: Date.now(),
      };

      await DDB.updateDevice(DDB_DEVICE_TABLE, device);
   }
};

exports.updateContacts = async(event) => {

   for (let i = 0; i < event.message_body.entries.length; i++) {
      var entry = event.message_body.entries[i];

      //trap for 0 on timestamps --bad tags
      if (entry.timestamp > 0) {
         //get last known exposure from db
         var contact = await PG.getContact(event.message_body.deviceId, entry.serial);

         //loop through scans and aggregate consecutive scans
         for (let j = 0; j < entry.logs.length; j++) {
            var scan = entry.logs[j];
            if (scan.recordType == 17) {

               //first time throuhg, seed the contact with 1st entry
               if (!contact) { contact = await this.createContact(event, entry, scan); }

               var scan_begin = entry.timestamp + (scan.log.delta * entry.scanInterval);
               var range = await this.parseRange(scan.log.rssi);

               //interpolate to determine if we extend this exposure or create a new one
               //break contacts into range buckets
               if ((scan_begin > (contact.event_end + process.env.INTERPOLATE)) || (contact.range != range)) {

                  //close exposure
                  contact.contact_status = 'closed';

                  await PG.saveContact(contact);
                  contact = await this.createContact(event, entry, scan);

               }
               else {

                  //trap for duplicate sends
                  if (scan_begin + entry.scanInterval > contact.event_end) {
                     //extend end date
                     contact.event_end = scan_begin + entry.scanInterval;

                     //accrue time in this distance range
                     contact.duration += entry.scanInterval;
                  }
               }
            }
         }
         //save any in-progress contact
         if (contact) {
            await PG.saveContact(contact);
         }
      }
   }
};


exports.createContact = async(event, entry, scan) => {

   var contact = {
      tag_a: event.message_body.deviceId,
      tag_b: entry.serial,
      event_start: entry.timestamp + (scan.log.delta * entry.scanInterval),
      event_end: (entry.timestamp + (scan.log.delta * entry.scanInterval)) + entry.scanInterval,
      range: await this.parseRange(scan.log.rssi),
      duration: entry.scanInterval,
      status: 'open'
   };

   return contact;
};



exports.parseBattery = async(batteryLevel) => {
   if (batteryLevel > 3000) { return 100; }
   else if (2900 < batteryLevel && batteryLevel < 3000) { return 80; }
   else if (2850 <= batteryLevel && batteryLevel < 2900) { return 60; }
   else if (2800 <= batteryLevel && batteryLevel < 2850) { return 40; }
   else if (2700 <= batteryLevel && batteryLevel < 2800) { return 20; }
   else if (batteryLevel <= 100) { return batteryLevel; }
   else { return 10; }
};

exports.parseRange = async(rssi) => {
   if (-35 <= rssi && rssi <= 0) { return 0 } //very close
   if (-60 <= rssi && rssi < -35) { return 1 } //close
   if (-99 <= rssi && rssi < -60) { return 2 } //mid-range
   if (rssi < -99) { return 3 } //far
   return 999; //unknown
};


exports.parseMessage = async(data) => {
   const Parser = require('binary-parser').Parser;

   const reverseBytes = (input) => {
      let result = '';
      for (let i = 0; i < input.length; i = i + 2) {
         result = input.substr(i, 2) + result;
      }
      return result;
   };

   let buf = Buffer.from(data, 'base64');
   const protocolVersion = buf.readUInt16LE(0);
   // console.log(protocolVersion)

   // Start POM Parser 2
   // parses each 8 byte log
   const logParser8 = new Parser()
      .endianess('little')
      .seek(2) // 2 bytes reserved
      .uint16('delta') // scan count since first record timestamp
      .int8('rssi')
      .uint8('motion')
      .int8('txPower');

   // parses each 4 byte log
   const logParser4 = new Parser()
      .endianess('little')
      .int8('rssi')
      .uint8('motion')
      .int8('txPower');

   const logParser = new Parser()
      .endianess('little')
      .uint8('recordType')
      .choice('log', {
         tag: 'recordType',
         choices: {
            16: logParser4,
            17: logParser8
         }
      });

   // array of entries
   const entryParser = new Parser()
      // log entry header
      .endianess('little')
      .uint8('entryStart') //A5
      .uint8('flags')
      .uint16('scanInterval')
      .string('serial', {
         encoding: 'hex',
         length: 6,
         formatter: reverseBytes
      })
      .uint32('timestamp') //30
      .uint16('length'); //32

   if (protocolVersion === 2) {
      entryParser.array('logs', {
         type: logParser,
         lengthInBytes: function() {
            return this.length - 16;
         }
      });
   }
   else if (protocolVersion === 1) {
      entryParser.array('logs', {
         type: logParser,
         readUntil: 'eof'
      });
   }

   // parses the main payload
   const pomParser = new Parser()
      .endianess('little')
      // header
      .uint16('entryProtocolVersion') //2
      .string('deviceId', { //8
         encoding: 'hex',
         length: 6,
         formatter: reverseBytes
      })
      .uint32('deviceTime') //12
      .uint32('lastUploadTime'); //16

   const headerVersionCheckByte = buf.readUInt8(16);
   // For latest header version, add some fields
   // 0xA5 at pos 16 means this is prior v2 protocol
   // without battery_level, network_id and fw_version
   // otherwise we do add parsers for those fields here

   if (headerVersionCheckByte != 0xA5) {
      pomParser
         .string('fwVersion', { //16
            encoding: 'hex',
            length: 4
         })
         .uint8('batteryLevel', { formatter: function(val) { return (val << 4); } }) //20 (left-shift by 4 to re-scale to millivolts)
         .uint16('networkId'); //22
   }

   pomParser
      .array('entries', {
         type: entryParser,
         readUntil: 'eof'
      });

   try {
      let jsonPayload = pomParser.parse(buf);
      return jsonPayload;
   }
   catch (error) {
      console.error(error);
   }

};
