'use strict';
const AWS = require('aws-sdk');
const DDB = new AWS.DynamoDB();

exports.updateDevice = async(table, device) => {

   //build update expression
   var update_expression = 'SET ';
   var ExpressionAttributeValues = {};

   for (var key in device) {
      if (device.hasOwnProperty(key)) {
         if (key != 'device_id') {
            update_expression += (key + '=:' + key + ',');
            ExpressionAttributeValues[':' + key] = { S: device[key].toString() };
         }
      }
   }
   //strip trailing comma
   update_expression = update_expression.slice(0, -1);

   var params = {
      TableName: table,
      Key: { device_id: { S: device.device_id } },
      UpdateExpression: update_expression,
      ExpressionAttributeValues: ExpressionAttributeValues,
      ReturnValues: 'ALL_NEW'
   };

   return await DDB.updateItem(params).promise();
};

exports.updateExposure = async(table, exposure) => {

   var update_expression = 'SET ';
   update_expression += 'exposure_end=:exposure_end,';
   update_expression += 'device_a=:device_a,';
   update_expression += 'device_b=:device_b,';
   update_expression += 'min_range=:min_range,';
   update_expression += 'max_range=:max_range,';
   update_expression += 'duration_range_0=:duration_range_0,';
   update_expression += 'duration_range_1=:duration_range_1,';
   update_expression += 'duration_range_2=:duration_range_2,';
   update_expression += 'duration_range_3=:duration_range_3,';
   update_expression += 'duration_range_999=:duration_range_999,';
   update_expression += 'exposure_status=:exposure_status,';
   update_expression += 'updated_at=:updated_at,';
   update_expression += 'expires_at=:expires_at';

   var params = {
      TableName: table,
      Key: {
         exposure_id: { S: exposure.exposure_id },
         exposure_begin: { N: exposure.exposure_begin.toString() }
      },
      UpdateExpression: update_expression,
      ExpressionAttributeValues: {
         ":exposure_end": { "N": exposure.exposure_end.toString() },
         ":device_a": { "S": exposure.device_a },
         ":device_b": { "S": exposure.device_b },
         ":min_range": { "N": exposure.min_range.toString() },
         ":max_range": { "N": exposure.max_range.toString() },
         ":duration_range_0": { "N": exposure.duration_range_0.toString() },
         ":duration_range_1": { "N": exposure.duration_range_1.toString() },
         ":duration_range_2": { "N": exposure.duration_range_2.toString() },
         ":duration_range_3": { "N": exposure.duration_range_3.toString() },
         ":duration_range_999": { "N": exposure.duration_range_999.toString() },
         ":exposure_status": { "S": exposure.exposure_status },
         ":updated_at": { "N": exposure.updated_at.toString() },
         ":expires_at": { "N": exposure.expires_at.toString() }
      },
      ReturnValues: 'ALL_NEW'
   };

   console.log('updating exposure: %j', params);
   return await DDB.updateItem(params).promise();

};


exports.updateExposure_OLD = async(table, exposure) => {

   //build update expression
   var update_expression = 'SET ';
   var ExpressionAttributeValues = {};

   for (var key in exposure) {
      if (exposure.hasOwnProperty(key)) {
         if (key != 'exposure_id' && key != 'exposure_begin') {
            update_expression += (key + '=:' + key + ',');

            if (key == 'exposure_status') { //extract the strings
               ExpressionAttributeValues[':' + key] = { S: exposure[key].toString() };
            }
            else {
               ExpressionAttributeValues[':' + key] = { N: exposure[key].toString() };
            }


         }
      }
   }
   //strip trailing comma
   update_expression = update_expression.slice(0, -1);

   var params = {
      TableName: table,
      Key: {
         exposure_id: { S: exposure.exposure_id },
         event_time: { N: exposure.exposure_begin.toString() }
      },
      UpdateExpression: update_expression,
      ExpressionAttributeValues: ExpressionAttributeValues,
      ReturnValues: 'ALL_NEW'
   };

   return await DDB.updateItem(params).promise();

};


exports.getOpenExposure = async(table, tagA, tagB) => {

   var params = {
      TableName: table,
      IndexName: 'exposure_id-exposure_status-index',
      KeyConditionExpression: "exposure_id=:exposure_id AND exposure_status=:exposure_status ",
      ExpressionAttributeValues: {
         ":exposure_id": { "S": tagA + '-' + tagB },
         ":exposure_status": { "S": "open" }
      },
      ScanIndexForward: false
   };

   var result = await DDB.query(params).promise();

   if (!result.hasOwnProperty('Items')) {
      //no open exposure found
      return false;
   }
   else {
      //found open exposure, first one should be latest
      if (result.Items.length > 1) {
         console.log('ERROR: found %s open exposures: %S', result.Items.length, result.items);
      }

      return result.Items[0];
   }

};
